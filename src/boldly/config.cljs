(ns boldly.config)


;; the environment of this project:
;; possible values are :dev and :prod
;; please change to :prod everytime you build the project for deployment and change back for developing

;; PLEASE REMEMBER TO RESTART THE APP EVERYTIME YOU CHANGE THIS (because otherwise Mixpanel won't realize the change!)

(def env :dev)

(def debug? (= env :dev))

(def mixpanel-tokens
  {:dev  "c7916be86d86fad0073cfe4712f55aca"
   :prod nil})

(defn mixpanel-token []
	(if-let [mp-token (mixpanel-tokens env)]
		mp-token
		(js/console.error "No Mixpanel tokens configured. Please add you Mixpanel tokens to 'boldly.config")))

(def firebase-cfgs
	{:dev {:uri "qjdkh5hacuo110ahaxfq22kh" :key "AIzaSyDJCiZaTleFw64UhBywbV5os1CPI9PL7lE"}
	:prod nil})

(defn firebase-cfg []
	(if-let [fb-cfg (firebase-cfgs env)]
		fb-cfg
		(js/console.error "No firebase url configured. Please add you firebase config to 'boldly.config")))

(def password "iuabuiz8")
