(ns boldly.handlers
  (:require
    [re-frame.core :refer [register-handler after dispatch dispatch-sync subscribe] :as rf]
    [schema.core :as s :include-macros true]
    [boldly.db :as db]
    [boldly.schema :as ms]
    [boldly.helpers :as h]
    [boldly.config :as cfg]
    [boldly.ios.ui :as ui]
    [boldly.firebase :as fb]
    [matchbox.core :as m]))

;; -- Middleware ------------------------------------------------------------
;;
;; See https://github.com/Day8/re-frame/wiki/Using-Handler-Middleware
;;


;; -- MixPanel related parts & handlers --------------------------------------------------------------

(def mixpanel
  (doto (js/require "react-native-mixpanel")
    (.sharedInstanceWithToken (cfg/mixpanel-token))))


(def mixpanel-event-names
  {:nav/createchallenge    "ENTER CHALLENGE CREATION"
   :add-challenge          "CREATE CHALLENGE"
   :nav/edit-challenge     "ENTER CHALLENGE EDITING"
   :edit-challenge         "EDIT CHALLENGE"
   :nav/challengedetail    "ENTER CHALLENGE DETAILS"
   :nav/chat               "ENTER CHAT"
   :nav/challenges         "LIST USER CHALLENGES"
   :nav/track              "ENTER SCORES"
   :track                  "SAVE SCORES"
   :chat/mp-track-message  "CHAT SEND MESSAGE"
   :join-challenge         "JOIN CHALLENGE"
   :open-share-dialog      "OPEN SHARE DIALOG"
   :write-user-to-local-db "REGISTER"
   })


(defn handler-val->mp-val [value]
  (if (map? value)
    (clj->js value)
    #js{:value (clj->js value)}))

(defn track-mixpanel [db [handler value]]
  (let [mp-name (or (mixpanel-event-names handler)
                    (str handler))]
    (if value
      (.trackWithProperties mixpanel mp-name (handler-val->mp-val value))
      (.track mixpanel mp-name))))

(defn track-mixpanel-diff-mw
  [handler-fn]
  (fn [db pvec]
    (let [result (handler-fn db pvec)
          diff (-> db
                   (clojure.data.diff result)
                   second
                   (dissoc :local)
                   (h/flatten-map ".")
                   clj->js)
          vecfirst (first pvec)]

      (track-mixpanel db [vecfirst diff])
      result)))

(def track-mixpanel-mw
  (after track-mixpanel))

;; -- middleware --------------------------------------------------------------

(defn check-and-throw
  "throw an exception if db doesn't match the schema."
  [a-schema db]
  identity
  #_(if-let [problems (s/check a-schema db)]
    (throw (js/Error. (str "schema check failed: " problems)))))

(def validate-schema-mw
  (when cfg/debug? (after (partial check-and-throw ms/DB))))

(def persist-db-mw
  (after db/persist-db!))

(def debug-handlers?
  false)

(def debug
  (when cfg/debug?
    rf/debug))




;; --- dev helpers:


(register-handler
  :reset
  ; a complete reset to the initial db
  [debug validate-schema-mw persist-db-mw]
  (fn [db [_ value]]
    (assoc-in db/initial-db [:local :loading] false)))

(register-handler
  :fetch-everything
  [debug]
  (fn [db]
    (m/deref fb/root #(dispatch [:reset-persisted-db {:firebase %}]))
    db))

(register-handler
  :fb-set-user-challenges
  [debug]
  (fn [db [_ uid value]]
    (assoc-in db [:firebase :users-challenges (keyword uid)] value)))

(register-handler
  :reset-to-firebase
  [debug]
  (fn [db]
    (m/deref (m/get-in fb/root [:users-challenges (-> db :user-id)])

                #(dispatch [:fb-set-user-challenges (-> db :user-id) %]))
    (dispatch [:init-fb-listeners])
    db))

(register-handler
  :switch-user
  [debug validate-schema-mw persist-db-mw]
  (fn [db [_ username]]
    (let [user-map { :lx        "ad40d49f-3ad8-4b77-8574-bb5446fa0c19"
                     :damiano   "4718807c-2f01-4edd-b9f9-c44fcb971cd3"
                     :gunnar    "4d4dc5bd-9569-43cb-b849-bc0807f808ac"
                    }]
      (dispatch [:fetch-everything])
      (dispatch [:init-fb-listeners])

      (assoc-in db [:user-id] (user-map username)))))

(register-handler
  :log-db
  [persist-db-mw]
  (fn [db [_ value]]
    (js/console.log (str (get-in db value)))
    db))


(register-handler
  :log-error
  [validate-schema-mw persist-db-mw]
  (fn [db [_ err-name data]]
    (js/console.log "Error" err-name (str data))
    db))


;; -- Database Initialization --------------------------------------------------------------

(register-handler
  :initialize-db
  [debug validate-schema-mw]
  (fn [_ _]
    (db/load-db (fn [v]
                  (dispatch [:reset-persisted-db v])
                  (dispatch [:set-loading false])
                  (dispatch [:init-firebase])
                  (dispatch [:init-fb-auth])))
    db/initial-db))

(register-handler
  ; called after recieving the full database from the store
  ; save the whole new db value except :local
  :reset-persisted-db
  [debug]
  (fn [db [_ value]]
    (merge db value)))


(register-handler
  :set-date
  [debug]
  (fn [db [_ _]]
    (assoc-in db
              [:local :date]
              (h/get-now-date))))

(register-handler
  :handle-initial-url-after-auth
  [debug]
  (fn [db [_ error]]
    (.then (.getInitialURL (.-Linking ui/react-native))
           #(when %
              (dispatch [:handle-open-url (str %)])))
    (assoc-in db [:local :connected] (not error))))

(register-handler
  :init-fb-auth
  [debug]
  (fn [db [_ _]]
    (when-let [user (get-in db [:firebase :users (-> db :user-id keyword)])]
      (js/setTimeout ; FIXME: hack to wait for establishing connection with firebase. We should fix m/auth and wait for authentication callback to do this
        #(dispatch [:handle-initial-url-after-auth nil]) 1000)
      #_(m/auth ; FIXME: broken in firebase v3 - needs fix in matchbox
        fb/root
        (:email user)
        cfg/password
        #(dispatch [:handle-initial-url-after-auth %1])))
    db))



;; -- Firebase Initialization --------------------------------------------------------------

(register-handler
  :set-connected
  [debug]
  (fn [db [_ value]]
    (assoc-in db [:local :connected] value)))

(register-handler
  :set-firebase
  [debug validate-schema-mw persist-db-mw]
  (fn [db [_ value]]
    (assoc db :firebase value)))

(register-handler
  :reset-firebase
  [debug validate-schema-mw persist-db-mw]
  (fn [db _]
    (assoc db :firebase nil)))

(def unsub-init-firebase
  "to make :init-firebase idempotent we always need to unsubscribe the old listener
  before we attach a new one
  we are storing the unsubscribe fn in this atom"
  (atom nil))

(register-handler
  :init-firebase
  [debug]
  (fn [db _]
    (when-let [user-id (-> db :user-id keyword)]
      (let [firebase (-> db :firebase)
            challenges (:challenges firebase)]

        ;; first push everything from the local db that the current user can change

        ;; push users-challenges
        (fb/reset-in! [:users-challenges user-id] (-> firebase :users-challenges user-id))

        ;; push user-profile
        (fb/reset-in! [:users user-id] (-> firebase :users user-id))

        ;; push all challenges that this user created

        (doseq [cv challenges
                :let [cid (keyword (key cv))
                      c (val cv)]]
          (fb/reset-in! [:trackings cid user-id] (-> firebase :trackings cid user-id))
          (when (= (-> c :created-by keyword) user-id)
            (fb/reset-in! [:challenges cid] (-> firebase :challenges cid)))
          (when (-> firebase :participants cid keys set user-id)
              (fb/reset-in! [:participants cid user-id] true))))

      (dispatch [:init-fb-listeners]))
    db))


(register-handler
  :fb-set-challenge
  [debug persist-db-mw]
  (fn [db [_ cid c]]
    (assoc-in db [:firebase :challenges cid] c)))

(register-handler
  :fb-set-user
  [debug persist-db-mw]
  (fn [db [_ uid user]]
    (assoc-in db [:firebase :users uid] user)))

(def users-listeners
  (atom #{}))

(register-handler
  :add-fb-user-listener
  [debug]
  (fn [db [_ cid uid]]
    (when (not= uid (:user-id db)) ; dont set listeners to myself

      ;; first listen to the user-profile of each of these users
      ;; but not when there is already this listener!
      (when-not (@users-listeners uid)
        (m/listen-to (m/get-in fb/root [:users uid])
                     :value
                     #(dispatch [:fb-set-user uid (second %)]))
        (swap! users-listeners conj uid)))
    db))

(register-handler
  :fb-set-msg
  [debug persist-db-mw]
  (fn [db [_ cid mid msg]]
    (assoc-in db [:firebase :messages cid mid] msg)))

(register-handler
  :fb-set-tracking
  [debug persist-db-mw]
  (fn [db [_ cid uid tracking]]
    (assoc-in db [:firebase :trackings cid uid] tracking)))

(register-handler
  :fb-set-participant
  [debug persist-db-mw]
  (fn [db [_ cid uid]]
    (assoc-in db [:firebase :participants cid uid] true)))

(register-handler
  :add-fb-challenge-listener
  [debug]
  (fn [db [_ cid]]

    ;; listen to challenge changes:
    (m/listen-to (m/get-in fb/root [:challenges cid]) :value #(dispatch [:fb-set-challenge cid (second %)]))

    ;; listen to :participants changes for this challenge
    (fb/listen-children
      [:participants cid]
      (fn [[event-type [uid _]]]
        (when (= event-type :child-added)
          (dispatch [:fb-set-participant cid (keyword uid)])
          (dispatch [:add-fb-user-listener cid (keyword uid)]))))

    ;; listen to all messenges of this challenge
    (fb/listen-children
      [:messages cid]
      (fn [[event-type [mid msg]]]
        (when (#{:child-added :child-changed} event-type)
          (dispatch [:fb-set-msg cid (keyword mid) msg]))))

    ;;listen to trackings of this challenge
    (fb/listen-children
      [:trackings cid]
      (fn [[event-type [uid trackings]]]
        (when (#{:child-added :child-changed} event-type)
          (dispatch [:fb-set-tracking cid (keyword uid) trackings]))))
    db))

(def users-challenges-listeners
  (atom #{}))

(register-handler
  :init-fb-listeners
  [debug]
  (fn [{:keys [user-id] :as db} _]

    (m/listen-to (m/get-in fb/root [:.info :connected])
                 :value
                 #(dispatch [:set-connected (second %)]))

    (when-not (@users-challenges-listeners user-id)
      (fb/listen-children
        [:users-challenges user-id]
        (fn [[event-type [cid _]]]
          (when (#{:child-added} event-type)
            (dispatch [:add-fb-challenge-listener (keyword cid)])))))
    db))

;; -- Registration --------------------------------------------------------------


(register-handler
  :set-user-name
  [debug validate-schema-mw persist-db-mw]
  (fn [db [_ value]]
    (let [user { :id nil :username value :email (str value "@boldly.io")}]
      (fb/create-user user
                      #(do
                         (dispatch-sync [:write-user-to-local-db %])
                         (dispatch [:init-firebase]))
                      #(do
                         (dispatch [:set-loading false])
                         (ui/alert "Error" %1)
                         (dispatch [:log-error
                                    (str "Error registering user: " %1)
                                    {:code (.-code %2)
                                     :message (.-message %2)
                                     :details (.-details %2)}])))
      (dispatch [:set-loading true])
      db)))

(register-handler
  :write-user-to-local-db
  [debug track-mixpanel-mw]
  (fn [db [_ value]]
    (dispatch [:set-loading false])
    (-> db
      (assoc-in [:user-id] (:id value))
      (assoc-in [:firebase :users (keyword (:id value))] value))))

;; -- Navigation --------------------------------------------------------------

;; nav stolen from: https://github.com/vikeri/re-navigate/blob/master/src/navigator_cljs/handlers.cljs

(register-handler
  :nav/push
  [debug validate-schema-mw]
  (fn [db [_ value]]
    (-> db
        (update-in [:local :nav :routes] #(conj % value))
        (update-in [:local :nav :index] inc))))

(register-handler
  :nav/pop
  [debug validate-schema-mw]
  (fn [db [_ _]]
    (-> db
        (update-in [:local :nav :index] #(max 0 (dec %)))
        (update-in [:local :nav :routes] pop))))

(register-handler
  :nav/home
  [debug validate-schema-mw]
  (fn [db [_ _]]
    (-> db
        (assoc-in [:local :nav :index] 0)
        (assoc-in [:local :nav :routes] [(get-in db [:local :nav :routes 0])]))))

(register-handler
  :nav/add-btn
  [debug validate-schema-mw]
  (fn [db [_ btn]]
    (assoc-in db [:local :nav :routes (-> db :local :nav :index) :right-btn] btn)))

(register-handler
  :nav/createchallenge
  [debug track-mixpanel-mw]
  (fn [db _]
    (dispatch [:nav/push {:title "New Challenge"
                          :scene-type :createchallenge
                          :key :createchallenge}])
    db))

(register-handler
  :nav/edit-challenge
  [debug track-mixpanel-mw]
  (fn [db [_ cid]]
    (dispatch [:nav/push {:title "Edit Challenge"
                          :scene-type :edit-challenge
                          :key (keyword (str "edit-challenge-" cid))
                          :cid cid}])
    db))

(register-handler
  :nav/challengedetail
  [debug track-mixpanel-mw]
  (fn [db [_ cid]]
    (let [created-by (get-in db [:firebase :challenges (keyword cid) :created-by])
          mine? (= created-by (-> db :user-id))]
      (dispatch [:nav/push (merge
                             {:title (:name @(subscribe [:challenge cid]))
                              :scene-type :challengedetail
                              :key (keyword (str "challengedetail-" cid))
                              :cid cid}
                             (when mine?
                               {:right-btn
                                {:text "Edit"
                                 :handler [:nav/edit-challenge cid]}}))]))
    db))

(register-handler
  :nav/chat
  [debug track-mixpanel-mw]
  (fn [db [_ cid]]
    (dispatch [:nav/push {:title (:name @(subscribe [:challenge cid]))
                          :scene-type :chat
                          :key (keyword (str "chat-" cid))
                          :cid cid}])
    db))


;; -- General Stuff --------------------------------------------------------------

(register-handler
  :set-loading
  [debug validate-schema-mw persist-db-mw]
  (fn [db [_ value]]
    (assoc-in db [:local :loading] value)))

(register-handler
  :appstate-change
  [debug]
  (fn [db [_ value]]
    (.track mixpanel (str "APP STATE CHANGED - " value))
    db))


;; -- Challenges --------------------------------------------------------------

(register-handler
  :add-challenge
  [debug validate-schema-mw persist-db-mw track-mixpanel-mw]
  (fn [db [_ data]]
    (let [data (-> data
                   (update :start-date #(if (integer? %) % (.getTime %)))
                   (update :end-date #(if (integer? %) % (.getTime %))))
          user-id (:user-id db)
          challenge (assoc data :created-by user-id)
          cid (fb/conj-in! :challenges challenge)
          cidk (keyword cid)
          uidk (keyword user-id)
          challenge (assoc challenge :id cid)]

      (fb/reset-in! [:challenges cidk] challenge)
      (fb/reset-in! [:participants cidk uidk] true)
      (fb/reset-in! [:users-challenges uidk cidk] true)

      (-> db
          (assoc-in [:firebase :challenges cidk] challenge)
          (assoc-in [:firebase :users-challenges uidk cidk] true)
          (assoc-in [:firebase :participants cidk uidk] true)))))

(defn change-nav-titles [pred title coll]
  (vec
    (map #(if (pred %)
            (assoc % :title title)
            %)
         coll)))

(register-handler
  :edit-challenge
  [debug validate-schema-mw persist-db-mw]
  (fn [db [_ challenge]]
    (let [challenge (-> challenge
                        (update :start-date #(if (integer? %) % (.getTime %)))
                        (update :end-date #(if (integer? %) % (.getTime %))))
          cid (:id challenge)
          title (:name challenge)
          nav-i-needs-change? #(and (= (:scene-type %) :challengedetail)
                                    (= (:cid %) cid))
          nav-items (change-nav-titles nav-i-needs-change? title (-> db :local :nav :children))]

      (fb/reset-in! [:challenges cid] challenge)

      (-> db
          (assoc-in [:local :nav :children] nav-items)
          (assoc-in [:firebase :challenges (keyword cid)] challenge)))))


;; -- Tracking --------------------------------------------------------------

(register-handler
  :track
  [debug validate-schema-mw persist-db-mw track-mixpanel-mw]
  (fn [db [_ {:keys [cid date value]}]]
    (let [uid (keyword (:user-id db))
          cid (keyword cid)
          challenge (-> db :firebase :challenges cid)
          today (-> db :local :date)
          date-key (h/date->key date)
          value (int value)

          old-trackings (get-in db [:firebase :trackings cid uid] {})
          new-trackings (assoc old-trackings date-key value)

          old-status (h/challenge->goal-status today challenge old-trackings)
          new-status (h/challenge->goal-status today challenge new-trackings)]

      (fb/reset-in! [:trackings cid uid date-key] value)

      (when (and new-status (not= old-status new-status))
        (case new-status
          :won           (ui/alert "Congratulations!" "You boldly rocked this challenge.")
          :daily-reached (ui/alert "Congratulations!" "You reached your daily goal! \nKeep up the good work.")))

      (assoc-in db [:firebase :trackings (keyword cid) (keyword uid) date-key] value))))



;; -- Linking --------------------------------------------------------------

(register-handler
  :handle-open-url
  [debug]
  (fn [db [_ url]]
    (if (-> db :local :connected)
      (let [cid (keyword (h/share-url->cid url))]
        (dispatch [:nav/home])
        (dispatch [:add-fb-challenge-listener cid])
        (dispatch [:set-loading true])
        (m/deref (m/get-in fb/root [:challenges cid])
                 #(do
                    (dispatch [:set-loading false])
                    (if %
                      (do
                        (dispatch [:fb-set-challenge cid %])
                        (dispatch [:nav/challengedetail cid])
                        (ui/alert "Invitation" (str "You have been invited to the challenge " (:name %) "!")))
                      (ui/alert "Sorry, this invitation link is not working!")))))
      (ui/alert "You must be connected to the internet to open Invitation Links!"))
    db))

;; -- Participants ---------------------------------------------------------

(register-handler
  :share-invitation
  [debug track-mixpanel-mw]
  (fn [db [_ success metho]] db))

(register-handler
  :share-invitation-error
  [debug track-mixpanel-mw]
  (fn [db error] db))

(register-handler
  :open-share-dialog
  [debug track-mixpanel-mw]
  (fn [db [_ challenge]]
    (let [connected?      (subscribe [:connected?])
          challenge-name  (:name challenge)
          username        (get-in db [:firebase :users (keyword (:user-id db)) :username])
          cid             (:id challenge)]
      (if @connected?
        (.showShareActionSheetWithOptions
          (.-ActionSheetIOS ui/react-native)
          #js{"subject" (str username " has challenged you!")
              "message" (str "I've invited you to a '" challenge-name "' challenge using Boldly!\nGet Boldly on the App Store here: http://bit.ly/boldlyapp\n\nJoin the challenge here: ")
              "url" (str "boldly://invitation?code=" cid)}
          #(dispatch [:share-invitation-error %1])
          #(dispatch [:share-invitation {:success %1 :method %2}]))

        (ui/alert "Invite Friends" (str "Sorry, you must be online to invite someone.")))
    db)))

(register-handler
  :join-challenge
  [debug persist-db-mw track-mixpanel-mw]
  (fn [db [_ cid]]
    (let [user-id (:user-id db)
          uidk (keyword user-id)
          cidk (keyword cid)]
      (fb/reset-in! [:users-challenges user-id cid] true)
      (fb/reset-in! [:participants cid user-id] true)
    (-> db
        (assoc-in [:firebase :users-challenges uidk cidk] true)
        (assoc-in [:firebase :participants cidk uidk] true)))))

;; -- Chat --------------------------------------------------------------

 (register-handler
  :chat/mp-track-message
   [track-mixpanel-mw]
   (fn [db [_ data]]
     db))

(register-handler
  :chat/send-message
  [debug]
  (fn [db [_ timestamp text cid]]
    (let [cid (keyword cid)
          msg {
                :id        nil
                :user-id   (:user-id db)
                :text      text
                :timestamp timestamp
                }
          mid  (fb/conj-in! [:messages cid] msg)
          cname (-> db :firebase :challenges cid :name)
          mp-tracking-data   (conj msg {:challenge-name cname})]
      (fb/reset-in! [:messages cid mid :id] mid)
      (dispatch [:chat/mp-track-message mp-tracking-data]))
    db))

(register-handler
  :update-message-read
  [debug persist-db-mw validate-schema-mw]
  (fn [db [_ cid]]
    (let [timestamp (.getTime (js/Date.))]
      (-> db
        (assoc-in [:messages-read (keyword cid)] timestamp)))))
