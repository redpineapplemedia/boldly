(ns boldly.firebase
  (:require [matchbox.core :as m]
            [boldly.config :as cfg]
            [clojure.string :as string]
            [re-frame.core :refer [dispatch]]
            [cljs-hash.goog :as gh]))

(defonce opts 
  (let [fb-cfg (cfg/firebase-cfg)]
    (m/init-web-options (:key fb-cfg) (:uri fb-cfg))))
(defonce app (m/init opts))
(defonce root (.ref (js/firebase.database)))
(defonce users (m/get-in root [:users]))

(def conj-in! (partial m/conj-in! root))
(def reset-in! (partial m/reset-in! root))

(def listen-to (partial m/listen-to root))
(def listen-children (partial m/listen-children root))
(defn format-pic
  [pic-str]

  (when pic-str (string/replace pic-str #"d=retro" "d=wavatar")))

(defn on-auth
  [cb-success user data]
    (let [user (-> user
                    (assoc :id (:uid data))
                    (assoc :pic (str "https://gravatar.com/avatar/" (gh/md5-hex (:email user)) "?d=wavatar")))]
      (m/reset-in! users [(:uid data)] user)
      (cb-success user)
      (dispatch [:handle-initial-url-after-auth])))

(defn- wrap-auth-cb [cb]
  (if cb
        (fn [err info]
          (cb err info))
        identity))

(defn fb-create-user
     "create-user creates a user in the Firebase built-in authentication server"
     [app email password & [cb]]
     (.. app
         auth
         (createUserWithEmailAndPassword email password)
         (then (wrap-auth-cb #(cb nil (js->clj (.toJSON %) :keywordize-keys true))))
         (catch (wrap-auth-cb #(cb {:code (.-code %) 
                                    :message (.-message %)} nil)))))

(defn create-user
  [user cb-success cb-error]
  (fb-create-user app (:email user) cfg/password
    #(if (nil? %1)
       (on-auth cb-success user %2)
       (case (keyword (.-code %1))
         :EMAIL_TAKEN (cb-error "Username is already taken" %1)
         :INVALID_EMAIL (cb-error "Username not allowed" %1)
         :NETWORK_ERROR (cb-error "Please check your internet connection" %1)
         (cb-error (str "Please check your username - " (.-code %1)) (merge %1 %2))))))
