(ns boldly.db
  (:require [boldly.ios.ui :as ui]
            [cognitect.transit :as t]
            [boldly.helpers :as h]
            ))




(def store (.-AsyncStorage ui/react-native))
(def store-key "db")

;; initial state of app-db
(def initial-db
  {:user-id nil
   :firebase {:users {}
     :challenges {}
     :trackings {}
     :users-challenges {}
     :messages {}
     :participants {}}
   :messages-read nil
   :local {:loading true
           :connected false
           :date (h/get-now-date)
           :nav     {:index 0
                     :key   :root
                     :routes [{:key :challenges
                               :root true
                               :title "Challenges"
                               :scene-type :challenges
                               :right-btn {:text "+"
                                           :handler [:nav/createchallenge]
                                           :font-size 30}}]}}})

(defn s->cljs [s]
  (t/read (t/reader :json) s))

(defn cljs->s [s]
  (t/write (t/writer :json) s))

(defn persist-db! [db]
  (-> store
      (.setItem store-key (-> db (dissoc :local) cljs->s)))) ; saves the whole db except :local in the store

(defn load-db [cb]
  (-> store
      (.getItem store-key)
      (.then #(cb (s->cljs %)))))

(defn flush-store []
  (-> store
      (.setItem store-key "")
      (.then #(js/console.log "store flushed !"))
      )
  )
