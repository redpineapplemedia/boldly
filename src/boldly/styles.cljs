(ns boldly.styles)

;; all styles coming here:

; color name are stolen from ionic1. see http://ionicframework.com/docs/components/#colors
(def colors {
             :light      "#a7aaa9"                          ; placeholders in input fields
             :stable     "#ffffff"                          ; text on dark backgrounds
             :positive   "#739F3d"                          ; Green - progressbar, winnerbadge
             :calm       "#757575"                          ; midgrey for some texts
             :balanced   "#ffffff"                          ; unused
             :energized  "#cf3721"                          ; Red - loserbadge
             :assertive  "#4897d8"                          ; navigation-background, tips
             :royal      "#ffdb5c"                          ; all buttons
             :dark       "#333333"                          ; userdata in formfields

             :background "#ffffff"                          ; background of page
             })

(def fonts {
            :main "Avenir"
            })

(def styles
  {:app                               {:position "absolute"
                                       :top      0
                                       :left     0
                                       :bottom   0
                                       :right    0}

   :default-scene                     {:margin-top       0
                                       :flex             1
                                       :background-color (:background colors)}

   :statusbar                         {:background-color (:background colors)
                                       :height           26}

   :toolbar                           {:position         "relative"
                                       :background-color (:background colors)}

   :footer { :height 60
            :border-top-width    1
            :border-color     (:light colors)
            :padding-top 10
            }

   :offline-message                   { :background-color (:energized colors)
                                        :padding-vertical 5 }

   :offline-message-text              { :text-align "center" :color (:background colors) }


   :scenes                            {
                                       ;:createchallenge  {:margin-top       (:margin-top (:default-scene styles))
                                       ;                   :flex             (:flex (:default-scene styles))
                                       ;                   :background-color (:background colors)}

                                       :register {
                                                  :flex        1
                                                  ;:background-color "red"
                                                  :align-items "center"
                                                  }
                                       }

   :searchfield                       {:border-color  (:lightgray colors)
                                       :height        30
                                       :color         (:dark colors)
                                       :margin        20
                                       :margin-top    100
                                       :padding-left  20
                                       :padding-right 20
                                       :border-width  1
                                       :border-radius 5}

   :create-challenge-input            {:height        40
                                       :color         (:dark colors)
                                       :font-family   (:main fonts)
                                       :text-align    "center"
                                       :padding-left  20
                                       :padding-right 20
                                       :margin        0}

   ; ********* navigation ***********

   :nav-header                        {
                                       :background-color (:assertive colors)
                                       }

   :nav-title                         {

                                       }

   :nav-title-text                    {
                                       :color       (:stable colors)
                                       :font-family (:main fonts)
                                       }

   :nav-right-btn                     {

                                       }

   :nav-right-btn-text                {
                                       :flex        1
                                       :font-size   18
                                       :font-weight "500"
                                       :color       (:royal colors)
                                       :text-align  "center"
                                       :font-family (:main fonts)
                                       }

   :nav-left-btn                      {}
   :nav-left-btn-icon                 {}



   ; ********* create challenge ***********

   :formfield-wrapper                 {:border-bottom-width 1
                                       :border-bottom-color (:assertive colors)}

   :create-challenge-multilineinput   {:height        80
                                       :font-size     17
                                       :color         (:dark colors)
                                       :font-family   (:main fonts)
                                       :padding-left  20
                                       :padding-right 20
                                       :margin        0
                                       }

   :create-challenge-input-display    {:height        40
                                       :font-size     17
                                       :padding-top   9
                                       :color         (:dark colors)
                                       :text-align    "center"
                                       :font-family   (:main fonts)
                                       :padding-left  20
                                       :padding-right 20
                                       :margin        0}

   :create-challenge-label-note       {:font-size     14
                                       :padding-top   9
                                       :color         (:assertive colors)
                                       :padding-left  20
                                       :margin-bottom 10}

   :button-full-size                  {;:width 80
                                       :padding-top      10
                                       :padding-right    30
                                       :padding-bottom   10
                                       :padding-left     30
                                       :margin-horizontal 20
                                       ;:border-radius    5
                                       :background-color (:royal colors)}

   :button-full-size-text             {
                                       :color       (:dark colors)
                                       :text-align  "center"
                                       :font-family (:main fonts)
                                       }

   :button-half-size                  {:flex 1}

   :button-half-size-touchable        {:padding-top      8
                                       :padding-right    30
                                       :padding-bottom   8
                                       :padding-left     30
                                       :background-color (:royal colors)}

   :button-half-size-text             {
                                       :color       (:dark colors)
                                       :text-align  "center"
                                       :font-family (:main fonts)
                                       }


   ; ********* challenges ***********

   :challenge-group-wrapper           {:border-top-width    1
                                       :border-bottom-width 1
                                       :border-color        (:assertive colors)
                                       :margin-bottom       0
                                       :margin-top          20
                                       :padding-bottom      5
                                       :padding-top         5
                                       :background-color    (:assertive colors)}

   :challenge-group                   {:text-align  "center"
                                       :color       (:stable colors)
                                       :font-family (:main fonts)}

   :challenge-li                      {
                                       :margin-bottom 20
                                       }

   :challenge-li-title-wrapper        {
                                       }

   :challenge-li-title                {
                                       :font-size     20
                                       :text-align    "center"
                                       :font-family   (:main fonts)
                                       :margin-top    10
                                       :margin-bottom 10
                                       }

   :challenge-li-details              {
                                       :padding-top    0
                                       :padding-bottom 0
                                       :border-width   1
                                       :border-color   (:assertive colors)
                                      ;  :background-color "lime"
                                       }

   :challenge-badge-unread-messages {
                                       :background-color (:energized colors)
                                       :border-radius 5
                                      ;  :width 20
                                       :position "absolute"
                                       :margin-top -140
                                       :right 5
                                       :padding-horizontal 5
                                       :padding-top 4
                                       :padding-bottom 1
                                       :flex-direction "row"
                                      }

   :challenge-badge-unread-messages-text {
                                       :color (:background colors)
                                       :text-align "center"
                                       :margin-left 5
                                      }

   :challenge-stats                   {
                                       :flex-direction "row"
                                       :height         145
                                       }

   :challenge-li-day                  {
                                       :flex        1
                                       :align-items "center"
                                       :padding-top 35
                                       ;:background-color "silver"
                                       }


   :challenge-li-day-label            {
                                       :text-align  "center"
                                       :color       (:calm colors)
                                       :font-family (:main fonts)
                                       :margin-top  5
                                       }

   :challenge-li-day-value            {
                                       :text-align  "center"
                                       :color       (:calm colors)
                                       :font-family (:main fonts)
                                       }

   ; ***** challenge goal (ringgraph) ******

   :challenge-goal                    {
                                       ;:align-items "center"
                                       :width      130
                                       ;:background-color "red"
                                       :margin-top 10
                                       :height 150
                                       }

   :ringgraph-progress                {:position "absolute"
                                       :width    140
                                       :height   140
                                       ;          :left 0
                                       ;:top 0
                                       ;:align-self "center"
                                       }

   :ringgraph-progress-small          {:position "absolute"
                                       :width    80
                                       :height   80
                                       }

   :ringgraph-progress-small-hairline {:position    "absolute"
                                       :width       72
                                       :height      72
                                       :margin-left 4
                                       :margin-top  4
                                       }

   :ringgraph-time                    {:position "absolute"
                                       :width    128
                                       :height   128
                                       :left     6
                                       :top      6
                                       }

   :ringgraph-overlay                 {:position "absolute"
                                       :width    120
                                       :height   120
                                       :top      10
                                       :left     10
                                       }

   :ringgraph-overlay-between         {:position "absolute"
                                       :width    130
                                       :height   130
                                       :top      5
                                       :left     5
                                       }


   :challenge-li-goal-text-wrapper    {
                                       :position    "absolute"
                                       :align-items "center"
                                       :left        0
                                       :top         30
                                       ;:background-color "orange"
                                       :width       140
                                       }

   :challenge-li-goal-label           {
                                       :text-align  "center"
                                       :font-size   17
                                       :font-family (:main fonts)
                                       :color       (:dark colors)
                                       }

   :challenge-li-goal-value           {
                                       :text-align  "center"
                                       :font-size   17
                                       :font-family (:main fonts)
                                       :color       (:calm colors)
                                       }

   :challenge-li-dailygoal            {
                                       :flex        1
                                       :align-items "center"
                                       ;:background-color "silver"
                                       :padding-top 35
                                       }

   :challenge-li-dailygoal-icon       {
                                       :margin-bottom 5
                                       :width         25
                                       }


   :challenge-li-dailygoal-label      {
                                       :text-align  "center"
                                       :color       (:calm colors)
                                       ;:background-color "orange"
                                       :font-family (:main fonts)
                                       :margin-top  5
                                       }

   :challenge-li-dailygoal-value      {
                                       :text-align  "center"
                                       :color       (:calm colors)
                                       :font-family (:main fonts)
                                       ;:background-color "purple"
                                       }


   ; *********** challenge details ****************



   :challenge-data                    {
                                       :padding-left     20
                                       :padding-right    20
                                       :padding-top      15
                                       :border-top-width 1
                                       :border-top-color (:assertive colors)
                                       }

   :challenge-note                    {
                                       :padding-vertical   10
                                       :padding-horizontal 20
                                       :border-top-width   1
                                       :border-top-color   (:assertive colors)

                                       }

   :challenge-note-label              {
                                       :font-size     16
                                       :color         (:assertive colors)
                                       :margin-bottom 10
                                       :font-family   (:main fonts)
                                       }

   :challenge-note-text               {
                                       :color       (:dark colors)
                                       :font-family (:main fonts)
                                       :font-size   16
                                       }

   :challenge-timeframe               {
                                       :margin-top         20
                                       :flex-direction     "row"
                                       :align-items        "flex-end"
                                       :padding-vertical   10
                                       :padding-horizontal 20
                                       :border-top-width   1
                                       :border-top-color   (:assertive colors)
                                       }

   :challenge-timeframe-icon          {
                                       :width        25
                                       :height       25
                                       :margin-right 5
                                       }

   :challenge-timeframe-label         {
                                       :font-size    16
                                       :color        (:assertive colors)
                                       :margin-right 20
                                       :font-family  (:main fonts)
                                       }

   :challenge-timeframe-text          {
                                       :font-size   16
                                       :color       (:dark colors)
                                       :font-family (:main fonts)
                                       }

   :challenge-participants            {
                                       :padding-vertical 10
                                       :border-top-width 1
                                       :border-top-color (:assertive colors)
                                       }

   :challenge-participants-data       {
                                       :flex-direction      "row"
                                       :align-items         "flex-end"
                                       :padding-horizontal  20
                                       :padding-bottom      10
                                       :border-bottom-width 1
                                       :border-bottom-color (:assertive colors)
                                       }

   :challenge-participants-icon       {
                                       :width        25
                                       :height       25
                                       :margin-right 5
                                       }

   :challenge-participants-label      {
                                       :font-size    16
                                       :color        (:assertive colors)
                                       :margin-right 20
                                       :font-family  (:main fonts)
                                       }

   :challenge-participants-value      {
                                       :font-size   16
                                       :color       (:dark colors)
                                       :font-family (:main fonts)
                                       }

   :challenge-participants-list       {
                                       :flex-direction "row"
                                       :padding-top    10
                                       :margin-bottom  10
                                       }

   :challenge-button                  {;:width 80
                                       :padding-top      10
                                       :padding-right    30
                                       :padding-bottom   10
                                       :padding-left     30
                                       :border-radius    5
                                       :margin-top       20
                                       :background-color (:royal colors)}

   ; *** tracking (part of challenge details) ***

   :join-button-wrapper               {
                                       :border-top-width 1
                                       :border-top-color (:assertive colors)
                                       :margin-top       0
                                       :padding-top      15
                                       ;:background-color "lime"
                                       :padding-left     20
                                       :padding-right    20
                                       }

   ; *** tracking (part of challenge details) ***

   :tracking                          {
                                       :border-top-width 1
                                       :border-top-color (:assertive colors)
                                       :margin-top       0
                                       :padding-top      15
                                       ;:background-color "lime"
                                       :padding-left     20
                                       :padding-right    20
                                       :padding-bottom   15
                                       }

   :tracking-wrapper                  {
                                       :flex-direction "row"
                                       :padding-top    10
                                       }

   :tracking-todays-score             {
                                       ;:background-color "purple"
                                       }

   :tracking-todays-score-label       {
                                       :font-size   16
                                       :color       (:assertive colors)
                                       :font-family (:main fonts)
                                       }

   :tracking-todays-score-value       {
                                       :font-size 16
                                       :color     (:dark colors)
                                       }

   :tracking-input-wrapper            {
                                       :flex         1
                                       :margin-right 10
                                       }

   :tracking-input                    {
                                       :border-color  (:light colors)
                                       :height        35
                                       ;:width         200
                                       :color         (:dark colors)
                                       :padding-left  20
                                       :padding-right 20
                                       :border-width  1
                                       :border-radius 5
                                       :text-align    "right"
                                       :font-family   (:main fonts)
                                       }


   ; *********** loading ****************

   :loading-text                      {
                                      :flex             1
                                      :font-size        20
                                      :color            (:dark colors)
                                      :font-weight      "bold"
                                      :font-family      (:main fonts)
                                      :background-color "rgba(0,0,0,0)"
                                      :margin-top       5
                                      }

   ; *********** register ****************

   :background-image                  {
                                       :flex       1
                                       :resizeMode "contain" ; "stretch" / "contain" / "cover"
                                       ;:position "absolute"
                                       }

   :register-form-label               {
                                       :flex             1
                                       :font-size        20
                                       :color            (:dark colors)
                                       :font-weight      "bold"
                                       :font-family      (:main fonts)
                                       :background-color "rgba(0,0,0,0)"
                                       ;:text-align "center"
                                       }

   :register-form-input               {:border-color  (:lightgray colors)
                                       :height        40
                                       :border-width  1
                                       :margin-top    5
                                       :padding-left  20
                                       :padding-right 20
                                       :border-radius 5}

   ; *********** get started ****************

   :gs-wrapper                        {
                                       :align-items    "flex-end"
                                       :flex-direction "row"
                                       ;:background-color "magenta"
                                       :margin-top     10
                                       }

   :gs-text                           {
                                       :flex         1
                                       ;:background-color "orange"
                                       :margin-right 10
                                       :font-size    18
                                       :text-align   "right"
                                       :font-family  (:main fonts)
                                       }

   :gs-arrow                          {
                                       ;:background-color "lime"
                                       :width         100
                                       :height        140
                                       :margin-right  20
                                       :margin-bottom 10

                                       }

   ; *********** others ****************

   :date-picker                       {
                                       ;:font-family (:main fonts) ; doesnt work
                                       ;:background-color "aqua"
                                       }


   :text1                             {:margin    20
                                       :font-size 30}})

; TODO: add a function to be able to have something like css classes
; maybe basscss port for cljsrn -> https://github.com/Project-J/lookbook
