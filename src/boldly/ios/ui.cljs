(ns boldly.ios.ui
  (:require [reagent.core :as r]
            [reagent.impl.component :as rc]
            [boldly.styles :refer [styles colors]]))

(def react-native (js/require "react-native"))
(def ionicons (js/require "react-native-vector-icons/Ionicons"))
(def fontawesome (js/require "react-native-vector-icons/FontAwesome"))
(def ionicon (r/adapt-react-class (.-default ionicons)))
(def faicon (r/adapt-react-class (.-default fontawesome)))

(def dismiss-keyboard (js/require "react-native-dismiss-keyboard"))

(def gifted-messenger (js/require "react-native-gifted-messenger/GiftedMessenger"))
(def gm (r/adapt-react-class (js/require "react-native-gifted-messenger/GiftedMessenger")))
(def communications (.-default (js/require "react-native-communications/AKCommunications")))

(def ProgressCircle (r/adapt-react-class (.-default (js/require "react-native-progress/Circle"))))
(def ProgressCircleSnail (r/adapt-react-class (.-default (js/require "react-native-progress/CircleSnail"))))

(declare view text touchable alert image switch window date-picker touchable-without-feedback scroll-view header-title
  navigation-header activity-indicator offline-message card-stack)


(def text (r/adapt-react-class (.-Text react-native)))
(def view (r/adapt-react-class (.-View react-native)))
(def image (r/adapt-react-class (.-Image react-native)))
(def touchable-highlight (r/adapt-react-class (.-TouchableHighlight react-native)))
(def touchable (r/adapt-react-class (.-TouchableOpacity react-native)))
(def touchable-without-feedback (r/adapt-react-class (.-TouchableWithoutFeedback react-native)))
(def date-picker (r/adapt-react-class (.-DatePickerIOS react-native)))
(def activity-indicator (r/adapt-react-class (.-ActivityIndicator react-native)))

(def switch (r/adapt-react-class (.-Switch react-native)))

(def card-stack (r/adapt-react-class (.-CardStack (.-NavigationExperimental react-native))))
(def navigation-header-comp (.-Header (.-NavigationExperimental react-native)))
(def navigation-header (r/adapt-react-class navigation-header-comp))
(def header-title (r/adapt-react-class (.-Title (.-Header (.-NavigationExperimental react-native)))))
(def dimensions (.-Dimensions react-native))
(def window (.get dimensions "window"))


(defn offline-message [msg]
  [view { :style (:offline-message styles)}
   [text { :style (:offline-message-text styles)}
    (str "You are not connected to the internet." (when-not (nil? msg) "\n") msg)]])

(defn alert
  ([title]
   (alert title nil nil))
  ([title msg]
   (alert title msg nil))
  ([title msg callback]
      (.alert (.-Alert react-native)
              title
              msg
              #js[#js{"text" "OK", "onPress" callback}])))

(def scroll-view (r/adapt-react-class (.-ScrollView react-native)))

(def input (r/adapt-react-class (.-TextInput react-native)))

(defn show-dialog [text message callback]
  (.prompt (.-AlertIOS react-native) text nil callback))

(defn focusable-input [init-attrs]
  "requires param :ref"
  (r/create-class
    {:display-name "focusable-input"
     :component-will-receive-props
                   (fn [this new-argv]
                     (let [ref-c (aget this "refs" (:ref init-attrs))
                           focus (:focus (rc/extract-props new-argv))
                           is-focused (.isFocused ref-c)]
                       (if focus
                         (when-not is-focused (.focus ref-c))
                         (when is-focused (.blur ref-c)))))
     :reagent-render
                   (fn [attrs]
                     (let [init-focus (:focus init-attrs)
                           auto-focus (or (:auto-focus attrs) init-focus)
                           attrs (assoc attrs :auto-focus auto-focus)]
                       [input attrs]))}))


