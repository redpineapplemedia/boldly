(ns boldly.ios.challenge-stats
  (:require-macros [reagent.ratom :refer [reaction]])
  (:require [reagent.core :as r]
            [re-frame.core :refer [subscribe dispatch]]
            [boldly.ios.ui :as ui :refer [view text image touchable alert input]]
            [boldly.styles :refer [styles colors]]
            [clojure.string :as string]
            [boldly.helpers :as h]))

(defn time-stats
  [cid]
  (let [challenge   (subscribe [:challenge cid])
        today       (subscribe [:date-today])]
    (fn []
      (let [time-status (h/challenge->time-status @today @challenge)
            days-ratio  (h/challenge->days-ratio @challenge @today)
            duration    (second days-ratio)
            dur-str     (h/pluralized-str duration "day")
            starts-in   (h/starts-in @today @challenge)
            day-label   (case time-status
                          :running  "Day"
                          :upcoming (str "lasts " dur-str)
                          :finished (str "lasted " dur-str))
            day-value   (case time-status
                          :running  (string/join "/" days-ratio)
                          :upcoming (str "starts in " (h/pluralized-str starts-in "day"))
                          :finished "finished")]
        [view {:style (styles :challenge-li-day)}
         [ui/faicon {:name "calendar" :size 25 :color (:assertive colors) }]
         [text {:style (styles :challenge-li-day-label)} day-label]
         [text {:style (styles :challenge-li-day-value)} day-value]]))))

(defn goal-circle
  [cid]
  (let [challenge     (subscribe [:challenge cid])
        today         (subscribe [:date-today])
        tracking-sum  (subscribe [:trackings/sum-for-challenge-and-user cid])
        ]
    (fn []
      (let [upcoming?     (h/upcoming? @today @challenge)
            days-ratio    (h/challenge->days-ratio @challenge @today)
            score-left    (max 0 (- (:goal @challenge) @tracking-sum))
            days-left     (- (second days-ratio) (first days-ratio))
            score-percent (/ @tracking-sum (:goal @challenge))
            time-percent  (/ (first days-ratio) (second days-ratio))
            ]
        [view {:style (styles :challenge-goal)}
         [view {:style {:height 150}}

          [ui/ProgressCircle
            {:animated false
             :progress score-percent
             :color (if (< score-percent time-percent )
                                         (:energized colors)
                                         (:positive colors))
             :unfilled-color "#eeeeee"
             :border-width 0
             :size 108
             :style {:padding-top 15
                     :margin-left 15}}]

          [ui/ProgressCircle
            {:animated false
             :progress time-percent
             :color (:assertive colors)
             :unfilled-color "#dddddd"
             :border-width 0
             :size 100
             :style {:margin-top -104
                     :margin-left 19}}]

          (if upcoming?
            [view {:style (styles :challenge-li-goal-text-wrapper)}
             [text {:style (styles :challenge-li-goal-label)} (str "Goal")]
             [text {:style (merge (styles :challenge-li-goal-value)
                                  {:margin-top 10})} (:goal @challenge)]]
            [view {:style (styles :challenge-li-goal-text-wrapper)}
             [text {:style (styles :challenge-li-goal-label)} (str "Goal")]
             [text {:style (styles :challenge-li-goal-value)} @tracking-sum]
             [text {:style (styles :challenge-li-goal-value)} (str "/ " (:goal @challenge))]])]]))))

(defn daily-stats
  [cid]
  (let [challenge     (subscribe [:challenge cid])
        today         (subscribe [:date-today])
        trackings     (subscribe [:trackings/for-challenge-and-user cid])]
    (fn []
      (let [goal-status (h/challenge->goal-status @today @challenge @trackings)
            running?    (h/running? @today @challenge)
            daily-goal-ratio (h/daily-goal-ratio @today @challenge @trackings)]
        (case goal-status
          :won  [view {:style (styles :challenge-li-dailygoal)}
                 [ui/ionicon {:name "ios-ribbon" :size 70 :color (:positive colors) }]]
          :lost [view {:style (styles :challenge-li-dailygoal)}
                 [ui/faicon {:name "times-circle" :size 70 :color (:energized colors) }]]
          [view {:style (styles :challenge-li-dailygoal)}
           [ui/faicon {:name "star"
                       :size 25
                       :color (if (= :daily-reached goal-status)
                                (:positive colors)
                                (:assertive colors))}]
           [text {:style (styles :challenge-li-dailygoal-label)} (str "Daily goal")]
           [text {:style (styles :challenge-li-dailygoal-value)}
            (if running?
              (string/join "/" daily-goal-ratio)
              (str (second daily-goal-ratio)))]])))))

(defn stats-component
  [cid]
  (let []
    (fn []
      [view {:style (styles :challenge-stats)}
       [time-stats cid]
       [goal-circle cid]
       [daily-stats cid]])))

