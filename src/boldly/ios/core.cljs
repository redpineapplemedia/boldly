(ns boldly.ios.core
  (:require [reagent.core :as r]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [boldly.handlers]
            [boldly.subs]
            [boldly.ios.ui :as ui :refer [view text image touchable alert input]]
            [boldly.navigation :refer [nav-root]]
            [boldly.ios.scenes.register :refer [register-scene]]
            [boldly.ios.scenes.loading :refer [loading-scene]]
            ))

(def app-registry (.-AppRegistry ui/react-native))

(defn app-root []
  (let [loading?      (subscribe [:loading?])
        user-exists?  (subscribe [:user-exists?])]
    (fn []
      (cond
        @loading?
          [loading-scene]
        (not @user-exists?) [register-scene]
        :else [nav-root]))))

(defn init []
  (dispatch-sync [:initialize-db])
  (.addEventListener (.-AppState ui/react-native ) "change" #(dispatch [:appstate-change %]))
  (.addEventListener (.-Linking ui/react-native) "url" #(dispatch [:handle-open-url (-> % js->clj (get "url"))]))

  (js/setInterval #(dispatch [:set-date]) 60000) ; update the current date every minute
  (.registerComponent app-registry "Boldly" #(r/reactify-component app-root)))
