(ns boldly.ios.scenes.chat
  (:require-macros [reagent.ratom :refer [reaction]])
  (:require [reagent.core :as r]
            [re-frame.core :refer [subscribe dispatch]]
            [boldly.ios.ui :as ui :refer [view text image touchable alert input date-picker switch focusable-input gifted-messenger gm communications window]]
            [boldly.styles :refer [styles colors]]
            [boldly.ios.challenge-stats :refer [stats-component]]
            [clojure.string :as string]
            [boldly.helpers :as h]))

(defn chat-scene
  [cid]
  (let [connected?  (subscribe [:connected?])
        messages    (subscribe [:chat/messages cid])
        ]
  (fn []

   ;with each new messages, we update the timestamp in :messages-read
   (dispatch [:update-message-read cid])

   [ui/view { :style (:default-scene styles) }
    (when-not @connected?
     [ui/offline-message "You can only read received messages."])
    [gm
     { :styles {
         :container   { }
         :bubbleLeft  { :background-color (:light colors) :margin-right 10 }
         :bubbleRight { :background-color (:assertive colors) :margin-left 10 }
         :name        { :color (:light colors) :font-size 12 :margin-left 55 :margin-bottom 5 }
         :image       { :align-self "center" :border-radius 20 :width 40 :height 40 }}
       :parseText                 true
       :autoFocus                 true
       :keyboardShouldPersistTaps false
       :handleSend                #(dispatch [:chat/send-message (.getTime (.-date %)) (.-text %) cid])
       :maxHeight                 (- (.-height window) 65)
       :displayNames              true
       :messages                  @messages
       :handleUrlPress            #(.web communications %)
       :handleEmailPress          #(.email communications #js[%] nil nil nil nil)
       :handlePhonePress          #(.phonecall communications % true)
       :hideTextInput             (not @connected?)}]])))
