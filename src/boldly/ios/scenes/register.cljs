(ns boldly.ios.scenes.register
  (:require [reagent.core :as r]
            [re-frame.core :refer [subscribe dispatch]]
            [boldly.ios.ui :as ui :refer [view text image touchable alert input]]
            [boldly.styles :refer [styles colors]]))

(def background-register-img (js/require "./images/registerBackground.png"))

(defn user-form []
  (let [new-name (r/atom "")]
    (fn []
      [input {:style (merge (styles :register-form-input) { :background-color (:background colors)})
              :max-length 30
              :on-change-text #(reset! new-name %)
              :default-value @new-name
              :placeholder "Set username"
              :clear-button-mode "always"
              :enables-return-key-automatically true
              :return-key-type "done"
              :multiline false
              :auto-focus true
              :auto-correct false
              :on-submit-editing #(dispatch [:set-user-name @new-name])}])))


(defn register-scene
  []
  [ui/view
   { :style (:register (:scenes styles)) }
    [ui/image {:source background-register-img  :style  (:background-image styles) }]
    [ui/view { :style { :position "absolute" :left 40 :right 40 :bottom 235 }} ; :width (.-width ui/window)
      [user-form]
     ]
   ]
)

