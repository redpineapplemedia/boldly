(ns boldly.ios.scenes.challengedetail
  (:require-macros [reagent.ratom :refer [reaction]])
  (:require [reagent.core :as r]
            [re-frame.core :refer [subscribe dispatch]]
            [boldly.ios.ui :as ui :refer [view text image touchable touchable-without-feedback alert input date-picker switch focusable-input scroll-view dismiss-keyboard]]
            [boldly.styles :refer [styles colors]]
            [boldly.ios.challenge-stats :refer [stats-component]]
            [clojure.string :as string]
            [boldly.helpers :as h]))

(defn timeframe
  [challenge]
  [view {:style (styles :challenge-timeframe)}
   [view {:style (styles :challenge-timeframe-icon)}
    [ui/faicon {:name "calendar" :size 25 :color (:assertive colors)}]]
   [text {:style (styles :challenge-timeframe-label)} "Timeframe"]
   [text {:style (styles :challenge-timeframe-text)}
    (str (h/to-short-format (h/js-date->cljs-date (:start-date @challenge)))
         " - "
         (h/to-short-format (h/js-date->cljs-date (:end-date @challenge))))]])

(defn chatbutton
  [challenge number-unread-messages]
  [view {:style (:button-full-size styles)}
   [touchable
    {:style    (:button-full-size-touchable styles)
     :on-press #(dispatch [:nav/chat (:id @challenge)])
     }
     (case @number-unread-messages
       0 [text {:style (:button-full-size-text styles)} "Chat"]
       1 [text {:style (:button-full-size-text styles)} (str "Chat (" @number-unread-messages " new message)")]
         [text {:style (:button-full-size-text styles)} (str "Chat (" @number-unread-messages " new messages)")]
       )
     ]])

(defn instructions
  [challenge]
  (when-not (nil? (:instructions @challenge))
    [view {:style (styles :challenge-note)}
     [text {:style (styles :challenge-note-label)} "Note"]
     [text {:style (styles :challenge-note-text)} (:instructions @challenge)]]))

(defn user-score-element
  [user challenge today]

  (let [tracking-sum  (:tracking-sum user)
        days-ratio    (h/challenge->days-ratio challenge today)
        score-left    (max 0 (- (:goal challenge) tracking-sum))
        score-percent (/ tracking-sum (:goal challenge))
        time-percent  (/ (first days-ratio) (second days-ratio))
        ]

    [ui/view
     {:style       {:width 80}
      :align-items "center"
      }

     [ui/ProgressCircle
            {:animated false
             :progress score-percent
             :color (if (< score-percent time-percent )
                                         (:energized colors)
                                         (:positive colors))
             :unfilled-color "#eeeeee"
             :border-width 0
             :size 60
             :style {:padding-top 10
                     :padding-bottom 10
                     :margin-left 0}}]

      [ui/image {:source       {:uri (:pic user)}
                :style         {:width 50 :height 50 :margin-top -65 :margin-bottom 10 :margin-left 0}
                :border-radius 25}]
     [ui/view
      [ui/text {:style {:text-align "center" :color (:assertive colors) :font-size 12}} (:username user)] ; {:style (:register-form-label styles) }
      [ui/text {:style {:text-align "center" :color (:light colors) :font-size 12}} (:tracking-sum user)]]]
    ))

(defn challenge-participants
  [challenge participating? participants-map]
  (let [number-participants   (count @participants-map)
        today                 (subscribe [:date-today])
        challenge-deref       @challenge
        today-deref           @today
        ]
    [view {:style (styles :challenge-participants)}
     [view { :style (styles :challenge-participants-data)}
       [view { :style (styles :challenge-participants-icon)}
        [ui/ionicon {:name "ios-person" :size 25 :color (:assertive colors) }]]
       [text {:style (styles :challenge-participants-label)} "Challengers"]
       [text {:style (styles :challenge-participants-value)} number-participants]]
     [ui/scroll-view { :horizontal true } ; todo: test if this horizontal scrolling actually works on a phone
      [ui/view { :style (styles :challenge-participants-list)}
        (for [v @participants-map]
                  ^{:key (:id v)}
                  [user-score-element v challenge-deref today-deref]
           )]]
        ]))

(defn tracking
  [challenge]
  (let [tracking-today (subscribe [:tracking-today-for-user (:id @challenge)])
        date           (subscribe [:date-today])
        value          (r/atom @tracking-today)]

    (fn []
      [view {:style (styles :tracking)}

       [view {:style (styles :tracking-todays-score)}
        [text {:style (styles :tracking-todays-score-label)} "Today´s Score: "]]

       [view {:style (styles :tracking-wrapper)}


        [view {:style (:tracking-input-wrapper styles)}
         [input
          {:ref                              "value"
           :keyboard-type                    "number-pad"
           :placeholder                      "enter score"
           :style                            (:tracking-input styles)
           :selectTextOnFocus                true
           :max-length                       6
           :clear-button-mode                "always"
           :default-value                    (str @value)
           :on-change-text                   #(reset! value %)
           ;:on-focus                         #(js/console.log "onfocus")
           }]]

        ; ****** Save Button **********************************************************

        [view {:style (:button-half-size styles)}
         [touchable
          {:style    (:button-half-size-touchable styles)
           :on-press #(dispatch [:track {:cid (:id @challenge) :date @date :value @value}])
           }

          [text
           {:style (:button-half-size-text styles)}
           (if (= 0 @tracking-today)
             "Save"
             "Update"
             )]]]
        ]

       ]
      )
    )
  )

(defn join-button
  [challenge]
  (fn []
    [view {:style (styles :join-button-wrapper)}
     [view {:style (:button-full-size styles)}
      [touchable
       {:style    (:button-full-size-touchable styles)
        :on-press #(dispatch [:join-challenge (:id @challenge)])
        }

       [text
        {:style (:button-full-size-text styles)}
        "Join"
        ]
       ]
      ]
     ]
    )
  )

(defn challengedetail-scene
  [cid]
  (let [connected?        (subscribe [:connected?])
        today             (subscribe [:date-today])
        challenge         (subscribe [:challenge cid])
        participating?    (subscribe [:challenge/participating? cid])
        participants-map  (subscribe [:participants-map-with-sum-for-challenge cid])
        number-unread-messages (subscribe [:chat/number-unread-messages cid])
        ]
    (fn []
      [ui/view {:style (:default-scene styles)}
       [ui/scroll-view {:keyboardShouldPersistTaps "always"}
        [touchable-without-feedback
         {:style    (:style {:background-color "aqua"})
          :on-press #(dismiss-keyboard)
          }
         [ui/view
          [stats-component (:id @challenge)]

          (if @participating?
            [view
             (when (h/running? @today @challenge) [tracking challenge])
             [chatbutton challenge number-unread-messages]
             ]
            [join-button challenge])

          [timeframe challenge]
          ]]
          [challenge-participants challenge participating? participants-map]
          [instructions challenge]
          ]
          (when @participating?
            [ui/view {:style (:footer styles)}
             [view {:style (:button-full-size styles)}
              [touchable
               {:style    (:button-full-size-touchable styles)
                :on-press #(dispatch [:open-share-dialog @challenge])
                }
               [text
                {:style (:button-full-size-text styles)} "Invite Friends"]]]])])))
