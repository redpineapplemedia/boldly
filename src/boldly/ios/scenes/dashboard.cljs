(ns boldly.ios.scenes.dashboard
  (:require [reagent.core :as r]
            [re-frame.core :refer [subscribe dispatch]]
            [boldly.ios.ui :as ui :refer [view text image touchable alert input]]
            [boldly.styles :refer [styles]]))


(defn dashboard-scene
  []
  (let [user-name (subscribe [:user-name])
        challenges-count (subscribe [:challenges/count])]
    (fn []
      [ui/view
       {:style (:default-scene styles)}
       [view {:style {:flex-direction "column" :margin 40 :align-items "center"}}
        [text {:style {:font-size 30 :font-weight "100" :margin-bottom 20 :text-align "center"}} (str "Hello " @user-name)]
        [touchable {:style {:background-color "#999" :padding 10 :border-radius 5 :margin-top 20}
                              :on-press #(dispatch [:nav/challenges])}
         [text {:style {:color "white" :text-align "center" :font-weight "bold"}} (str "See your " @challenges-count " Challenges")]]

        [touchable {:style {:background-color "#999" :padding 10 :border-radius 5 :margin-top 20}
                              :on-press #(dispatch [:nav/createchallenge])}
         [text {:style {:color "white" :text-align "center" :font-weight "bold"}} (str "setup Challenge")]]]])))

