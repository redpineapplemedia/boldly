(ns boldly.ios.scenes.loading
  (:require [reagent.core :as r]
            [re-frame.core :refer [subscribe dispatch]]
            [boldly.ios.ui :as ui :refer [view text image touchable alert input activity-indicator]]
            [boldly.styles :refer [styles colors]]))

(def background-register-img (js/require "./images/registerBackground.png"))

(defn loading-scene
  []
  [ui/view
   { :style (:register (:scenes styles)) }
    [ui/image {:source background-register-img  :style  (:background-image styles) }]
    [ui/view { :style { :position "absolute" :left 40 :right 40 :bottom 260 :align-items "center" }} ; :width (.-width ui/window)
     [activity-indicator {:size "large" :color (:dark colors)}]
     [ui/text {:style (:loading-text styles) } "...loading"]]])