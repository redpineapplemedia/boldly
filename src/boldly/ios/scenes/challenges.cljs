(ns boldly.ios.scenes.challenges
  (:require-macros [reagent.ratom :refer [reaction]])
  (:require [reagent.core :as r]
            [re-frame.core :refer [subscribe dispatch]]
            [boldly.ios.ui :as ui :refer [view text image touchable alert input]]
            [boldly.styles :refer [styles colors]]
            [clojure.string :as string]
            [boldly.helpers :as h]
            [boldly.ios.challenge-stats :refer [stats-component]]))

(def arrow-img (js/require "./images/arrow.png"))

(defn challenge-li
  [cid]
  (let [challenge               (subscribe [:challenge cid])
        number-unread-messages  (subscribe [:chat/number-unread-messages cid])]
    (fn []
      [touchable {:on-press #(dispatch [:nav/challengedetail cid])}
       [view {:style (styles :challenge-li)}
        [view {:style (styles :challenge-li-title-wrapper)}
         [text {:style (styles :challenge-li-title)} (:name @challenge)]]
        [view {:style (styles :challenge-li-details)}
         [stats-component cid]
         (when-not (= 0 @number-unread-messages)
         [view {:style (styles :challenge-badge-unread-messages)}
          [ui/ionicon {:name "md-chatboxes" :size 20 :color (:background colors) }]
          [ui/text {:style (styles :challenge-badge-unread-messages-text)}
          @number-unread-messages]])
         ]]])))

(defn label [txt]
  [view {:style (:challenge-group-wrapper styles)}
   [text {:style (:challenge-group styles)} txt]])

(defn challenge-list
  ([challenges]
   (challenge-list challenges nil))
  ([challenges label-txt]
   [view {:margin-top 5}
    (if (and label-txt @challenges) [label label-txt])
    (when @challenges
      (for [{:keys [id]} @challenges]
        ^{:key id}
        [challenge-li id]))]))


(defn challenges-scene
  []
  (let [cempty?     (subscribe [:challenges/empty?])
        current     (subscribe [:challenges/current])
        upcoming    (subscribe [:challenges/upcoming])
        finished    (subscribe [:challenges/finished])]
    (fn []
      [ui/view
       {:style (:default-scene styles)}
       (if @cempty?
         [view { :style (:gs-wrapper styles) }
          [text {:style  (:gs-text styles) } "Get Started!"]
          [image {:source arrow-img :resizeMode "stretch" :style  (:gs-arrow styles) }]]
         [ui/scroll-view
          [view

           [challenge-list current]
           [challenge-list upcoming (str "Upcoming Challenges:")]
           [challenge-list finished "Finished Challenges:"]]])])))
