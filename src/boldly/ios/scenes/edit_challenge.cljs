(ns boldly.ios.scenes.edit-challenge
  (:require [reagent.core :as r]
            [re-frame.core :refer [subscribe dispatch]]
            [boldly.ios.ui :as ui :refer [view text image touchable alert input date-picker switch focusable-input]]
            [boldly.styles :refer [styles colors]]
            [boldly.helpers :as h]))

(defn createchallenge-form [cid]
  (let [challenge                (subscribe [:challenge cid])
        focusedfield             (r/atom nil)
        data                     (r/atom (-> @challenge
                                           (update :start-date #(if (integer? %) (js/Date. %) %))
                                           (update :end-date #(if (integer? %) (js/Date. %) %))))
        errors                   (r/atom #{})
        st                       (:createchallenge (:scenes styles))
        seterror-required        (fn [field]
                                   (if (empty? (-> @data field str))
                                     (swap! errors conj field)
                                     (swap! errors disj field)))
        seterror-end-after-start (fn []
                                   (if (h/end-after-or-equal-start? (:start-date @data)
                                                                    (:end-date @data))
                                     (swap! errors disj :end-date)
                                     (swap! errors conj :end-date)))
        save-fn                  (fn []
                                   (do
                                     (seterror-required :goal)
                                     (seterror-required :name)
                                     (seterror-end-after-start)
                                     (if (empty? @errors)
                                       (do
                                         (dispatch [:edit-challenge @data])
                                         (dispatch [:nav/pop]))
                                       (reset! focusedfield (first @errors)))))]

    ;(dispatch [:nav/add-btn {:text "Save" :handler save-fn}])
    (fn []
      [ui/scroll-view {:keyboardShouldPersistTaps "always"}


       ; ****** Name **********************************************************

       [view {:style (merge (:formfield-wrapper styles)
                            (when (:name @errors)
                              { :border-color "red" :border-left-width 6}))}
        [input
         {:ref                    "name"
          :placeholder            "write title of challenge e.g. Burpees"
          :style                  (:create-challenge-input styles)
          :return-key-type        "done"
          :max-length             40
          :clear-button-mode      "always"
          :focus                  (= :name @focusedfield)
          :on-focus               #(reset! focusedfield :name)
          :on-blur                #(seterror-required :name)
          :on-submit-editing      #(reset! focusedfield :goal)
          :default-value          (:name @data)
          :select-text-on-focus   true
          :on-change-text         #(do (swap! data assoc :name %) (seterror-required :name))
          :enables-return-key-automatically true}]]

       ; ****** goal **********************************************************

       [view {:style (merge (:formfield-wrapper styles)
                            (when (:goal @errors)
                              { :border-color "red" :border-left-width 6}))}
        [input
         {:ref                     "goal"
          :keyboard-type           "number-pad"
          :placeholder             "set final number goal"
          :style                   (:create-challenge-input styles)
          :return-key-type         "done"
          :focus                   (= :goal @focusedfield)
          :on-blur                 #(seterror-required :goal)
          :clear-button-mode       "always"
          :on-focus                #(reset! focusedfield :goal)
          :default-value           (-> @data :goal str)
          :select-text-on-focus   true
          :on-change-text          #(do
                                      (swap! data assoc :goal
                                             (if (empty? %)
                                               nil
                                               (js/parseInt %)))
                                      (seterror-required :goal))
          :max-length              6
          :on-submit-editing       #(reset! focusedfield :start-date)
          :enables-return-key-automatically true}]]

       ; ****** Start Date **********************************************************

       [view {:style (:formfield-wrapper styles)}
        [text {:style (:create-challenge-input-display styles)
               :on-press #(do
                            (ui/dismiss-keyboard)
                            (if (= :start-date @focusedfield)
                              (reset! focusedfield nil)
                              (reset! focusedfield :start-date)))}
         (str "starting: " (h/to-us-format (h/js-date->cljs-date (:start-date @data))))]
        (when (= :start-date @focusedfield)
          [date-picker
           {:ref             "start-date"
            :mode            "date"
            :date            (:start-date @data)
            :on-date-change  #(swap! data assoc :start-date (h/at-noon %))}])]

       ; ****** End Date **********************************************************

       [view {:style (merge (:formfield-wrapper styles) (when (:end-date @errors) { :border-color "red" :border-left-width 6}))}
        [text {:style (:create-challenge-input-display styles)
               :on-press #(do
                            (ui/dismiss-keyboard)
                            (if (= :end-date @focusedfield)
                              (reset! focusedfield nil)
                              (reset! focusedfield :end-date)))}
         (str "ending: " (h/to-us-format (h/js-date->cljs-date (:end-date @data))))]
        (when (= :end-date @focusedfield)
          (do
            [date-picker {:ref                   "end-date"
                          :minimum-date          (:start-date @data)
                          :mode                  "date"
                          :date                  (:end-date @data)
                          :on-date-change        #(do (swap! data assoc :end-date (h/at-noon %)) (seterror-end-after-start))}]))]
       [focusable-input {
                          :ref "instructions"
                          :style (:create-challenge-multilineinput styles)
                          :placeholder "add a note (optional)" ;"Optional space for notes regarding the challenge. Rules, instructions or support!"
                          :multiline true
                          :focus (= :instructions @focusedfield)
                          :on-focus #(reset! focusedfield :instructions)
                          :on-blur #(reset! focusedfield nil)
                          :on-submit-editing #(reset! focusedfield nil)
                          :default-value (:instructions @data)
                          :select-text-on-focus   true
                          :on-change-text #(swap! data assoc :instructions %)}]

       ; ****** Submit Button **********************************************************

       [view {:style {:margin-bottom 255}}
        [touchable
         {:style (:button-full-size styles)
          :on-press save-fn}

         [text
          {:style (:button-full-size-text styles)}
          "Save"]]]])))

(defn edit-challenge-scene
  [cid]
  (fn []
    [ui/view
     {:style (:default-scene styles)}
     [createchallenge-form cid]]))
