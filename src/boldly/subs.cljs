(ns boldly.subs
  (:require-macros [reagent.ratom :refer [reaction]])
  (:require [re-frame.core :refer [register-sub subscribe]]
            [boldly.helpers :as h]))


;; General

(register-sub
  :loading?
  (fn [db _]
    (reaction
      (get-in @db [:local :loading]))))

(register-sub
  :connected?
  (fn [db _]
    (reaction
      (get-in @db [:local :connected]))))

(register-sub
  :registered?
  (fn [db _]
    (reaction
      (get-in @db [:local :registered]))))

(register-sub
  :date-today
  (fn [db _]
    (reaction
      (get-in @db [:local :date]))))

;; Navigation

(register-sub
  :nav/index
  (fn [db _]
    (reaction
      (get-in @db [:local :nav :index]))))

(register-sub
  :nav/state
  (fn [db _]
    (reaction
      (get-in @db [:local :nav]))))

(register-sub
  :nav/current
  (fn [db _]
    (reaction
      (get-in @db [:local :nav :routes (-> @db :local :nav :index)]))))

(register-sub
  :nav/by-idx
  (fn [db [_ idx]]
    (reaction
      (get-in @db [:local :nav :routes idx]))))


(register-sub
  :nav/by-key
  (fn [db [_ nkey]]
    (let [nav-childs (reaction (get-in @db [:local :nav :routes]))]
      (reaction
        (first (filter #(= nkey (:key %)) @nav-childs))))))



;; User:

(register-sub
  :user
  (fn [db _]
    (reaction
      (get-in @db [:firebase :users (keyword (:user-id @db))]))))

(register-sub
  :user-id-k
  (fn [db _]
    (reaction
      (keyword (:user-id @db)))))

(register-sub
  :user-exists?
  (fn [db _]
    (reaction
      (some? (:user-id @db)))))

(register-sub
  :user-name
  (fn [db _]
    (let [user (subscribe [:user])]
      (reaction
        (-> @user :username)))))



;; -------- Challenges -----------------------

(register-sub
  :all-challenges-map
  (fn [db _]
    (reaction
      (-> @db :firebase :challenges))))

(register-sub
  :challenges-map
  (fn [db _]
    (reaction
      (let [user-id-k (-> @db :user-id keyword)
            cids      (-> @db :firebase :users-challenges user-id-k keys)]
        (-> @db :firebase :challenges (select-keys cids))))))

(register-sub
  :challenges
  (fn [db _]
    (let [challenges-map (subscribe [:challenges-map])]
      (reaction
          (vals @challenges-map)))))

(register-sub
  :challenges/count
  (fn [db _]
    (let [challenges (subscribe [:challenges])]
      (reaction
        (-> @challenges count)))))

(register-sub
  :challenges/empty?
  (fn [db _]
    (let [cs (subscribe [:challenges])]
      (reaction (empty? @cs)))))

(register-sub
  :challenge
  (fn [db [_ id]]
    (let [challenges-map (subscribe [:all-challenges-map])]
      (reaction
        (get @challenges-map (keyword id))))))

(register-sub
  :challenge-title
  (fn [db [_ id]]
    (let [challenge (subscribe [:challenge id])]
      (reaction
        (:title @challenge)))))

(register-sub
  :challenge/participants
  (fn [db [_ cid]]
    (reaction
        (-> @db
            (get-in [:firebase :participants (keyword cid)])
            keys
            set))))

(register-sub
  :challenge/participating?
  (fn [db [_ cid]]
    (let [participants (subscribe [:challenge/participants cid])
          user-id (subscribe [:user-id-k])]
      (reaction
        (boolean (@participants @user-id))))))

(register-sub
  :challenge/daily-goal-ratio
  (fn [db [_ cid]]
    (let [trackings           (subscribe [:trackings/for-challenge cid])
          challenge           (subscribe [:challenge cid])
          today               (subscribe [:date-today])]
      (reaction (h/daily-goal-ratio @today @challenge @trackings)))))

(register-sub
  :challenges/current
  (fn [db]
    (let [today (subscribe [:date-today])
          challenges (subscribe [:challenges])
          current-challenges (reaction (filter (partial h/running? @today) @challenges))
          sorted-challenges (reaction (sort-by :start-date >= @current-challenges))]
    (reaction (not-empty @sorted-challenges)))))

(register-sub
  :challenges/upcoming
  (fn [db]
    (let [today (subscribe [:date-today])
          challenges (subscribe [:challenges])
          upcoming-challenges (reaction (filter (partial h/upcoming? @today) @challenges))
          sorted-challenges (reaction (sort-by :start-date < @upcoming-challenges))]
    (reaction (not-empty @sorted-challenges)))))

; ---------- Trackings -------------------------

(register-sub
  :challenges/finished
  (fn [db]
    (let [today (subscribe [:date-today])
          challenges (subscribe [:challenges])
          finished-challenges (reaction (filter (partial h/finished? @today) @challenges))
          sorted-challenges (reaction (sort-by :end-date >= @finished-challenges))]
    (reaction (not-empty @sorted-challenges)))))

(register-sub
  :trackings-for-challenge
  (fn [db [_ cid]]
    (reaction
        (-> @db :firebase :trackings (get (keyword cid))))))

(register-sub
  :trackings-all
  (fn [db _ ]
    (reaction (-> @db :firebase :trackings))))

(register-sub
  :trackings/for-challenge-and-user
  (fn [db [_ cid uid]]
    (let [user-id   (if uid
                      (reaction (-> uid))
                      (reaction (:user-id @db)))
          trackings (subscribe [:trackings-for-challenge cid])]
      (reaction (get @trackings (keyword @user-id) {})))))

(register-sub
  :tracking-today-for-user
  (fn [db [_ cid uid]]
    (let [tracking (subscribe [:trackings/for-challenge-and-user cid uid])
          day      (subscribe [:date-today])]
      (reaction (get @tracking (h/date->key @day) 0)))))

(register-sub
  :trackings/sum-for-challenge-and-user
  (fn [db [_ cid uid]]
    (let [trackings (subscribe [:trackings/for-challenge-and-user cid uid])]
      (reaction (->> @trackings vals (apply +))))))

; ------------ Participants ------------------

(register-sub
  :participants-map
  (fn [db [_ cid]]
      (reaction
        (let [user-ids (keys (get-in @db [:firebase :participants (keyword cid)]))]
          (select-keys (get-in @db [:firebase :users]) user-ids)))))

(defn assoc-tracking-sum
  [cid trc [uid user]]
  (let [sum (->> trc uid vals (apply +))]
    (assoc user :tracking-sum sum)))

(register-sub
  :participants-map-with-sum-for-challenge
  (fn [db [_ cid]]
    (let [participants (subscribe [:participants-map cid])
          trackings-c (subscribe [:trackings-for-challenge cid])
          ]
      (reaction
        (sort-by :tracking-sum >
          (map (partial assoc-tracking-sum cid @trackings-c) @participants) ;#(assoc (val %) :tracking-sum 300)
        ))
      )))


; ------------ Chat --------------------------

(defn add-gm-data [user-id participants msg]
  (let [user (-> msg :user-id keyword participants)]
    {
     :uniqueId  (:id msg)
     :text      (:text msg)
     :position  (if (= user-id (:user-id msg)) "right" "left")
     :name      (:username user)
     :image     { :uri (:pic user) }
     :date      (js/Date. (:timestamp msg)) }))

(register-sub
  :chat/raw-messages
  (fn [db [_ cid]]
    (reaction (vals (get-in @db [:firebase :messages (keyword cid)])))))

(register-sub
  :chat/messages
  (fn [db [_ cid]]
    (let [participants        (subscribe [:participants-map cid])
          user-id             (reaction (:user-id @db))
          challenge-messages  (subscribe [:chat/raw-messages cid])]
      (reaction
        (map (partial add-gm-data @user-id @participants)
          (sort-by :timestamp < @challenge-messages))))))

(register-sub
  :chat/last-message-read-at
  (fn [db [_ cid]]
    (reaction
      (get-in @db [:messages-read (keyword cid)]))))

(register-sub
  :chat/number-unread-messages
  (fn [db [_ cid]]
    (let [last-message-read-at (subscribe [:chat/last-message-read-at cid])
          messages             (subscribe [:chat/raw-messages cid])]
      (reaction
        (if (> (count @messages) 0)
          (count (filter #(-> % :timestamp (> @last-message-read-at)) @messages))
          0)))))
