(ns boldly.schema
  (:require [schema.core :as s :include-macros true]))




;; firebase schema

(defn optionalize-keys [m]
  "wraps all keys of map m with s/optional-key"
  (into {}
    (for [[k v] m]
      [(s/optional-key k) v])))

(def UserId
  s/Str)
(def UserIdKey
  s/Keyword)

(def ChallengeId
  s/Str)
(def ChallengeIdKey
  s/Keyword)

(def MessageId
  s/Str)
(def MessageIdKey
  s/Keyword)

(def DateKey
  "date as a keyword in the form: :2016-06-06"
  s/Keyword)

(def TrackingScore
  s/Num)

(def FirebaseDate
  "we safe our js Date objects as timestamps
  (you can use the js function .getTime for that)"
  s/Int)

(def FirebaseUser
  (optionalize-keys
    {:id UserId
     :pic (s/maybe s/Str)
     :email s/Str
     :username s/Str}))

(def FirebaseChallenge
  (optionalize-keys
    {:id ChallengeId
     :created-by UserId
     :name s/Str
     :goal s/Int
     :start-date FirebaseDate
     :end-date FirebaseDate
     :instructions (s/maybe s/Str)}))

(def Challenge FirebaseChallenge)

(def FirebaseTrackings
  {ChallengeIdKey
   {UserIdKey
    {DateKey TrackingScore}}})

(def FirebaseMessage
  (optionalize-keys
    {:id          MessageId
     :user-id     UserId
     :text        s/Str
     :timestamp   FirebaseDate}))

(def FirebaseMessages
  {ChallengeIdKey
   {MessageIdKey FirebaseMessage}})

(def FirebaseParticipants
  {ChallengeIdKey
   {UserIdKey s/Bool}})

(def FirebaseUsersChallenges
  {UserIdKey
   {ChallengeIdKey s/Bool}})

(def Firebase
  (optionalize-keys
    {:users {UserIdKey FirebaseUser}
     :challenges {ChallengeIdKey FirebaseChallenge}
     :trackings FirebaseTrackings
     :users-challenges FirebaseUsersChallenges
     :messages FirebaseMessages
     :participants FirebaseParticipants }))


;; schema of app-db

(def NavigationStateRightBtn
  {:text s/Str
   :handler (s/cond-pre (s/pred fn?) [s/Any])
   (s/optional-key :font-size) s/Int}) ; dispatch-parameter vector like [:nav/edit-challenge 5]

(def NavigationState
  {:key s/Keyword
   (s/optional-key :title) s/Str
   (s/optional-key :root) s/Bool
   :scene-type s/Keyword
   (s/optional-key :right-btn) NavigationStateRightBtn
   (s/optional-key :cid) s/Str})

(def NavigationParentState
  {:index    s/Int
   :key      s/Keyword
   :routes [NavigationState]})

(def Local
  {:loading     s/Bool
   :connected   s/Bool
   :date        (s/maybe s/Inst)
   :nav         NavigationParentState})


(def User
  {:id       s/Str
   :username s/Str
   :email    s/Str
   :pic      (s/maybe s/Str)})

(def ChallengeTrackings
  {s/Keyword s/Int}) ; {:2016-06-06:10}

(def MessagesRead
  {:cid       ChallengeIdKey
   :timestamp FirebaseDate})

(def DB
  {:user-id    (s/maybe UserId)
   :local      Local ; nothing in :local will be persisted to the phone store
   :firebase   (s/maybe Firebase)
   :messages-read (s/maybe MessagesRead)
   })


(def ChallengeGoalStatus
  (s/maybe (s/enum :won
                   :lost
                   :daily-reached)))

(def ChallengeTimeStatus
  (s/enum :running
          :upcoming
          :finished))
