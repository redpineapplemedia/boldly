(ns boldly.navigation
  (:require [reagent.core :as r]
            [re-frame.core :refer [subscribe dispatch]]
            [boldly.ios.ui :as ui :refer [header-title navigation-header]]
            ;[boldly.ios.scenes.dashboard :refer [dashboard-scene]]
            [boldly.styles :refer [styles colors]]
            [boldly.ios.scenes.challenges :refer [challenges-scene]]
            [boldly.ios.scenes.createchallenge :refer [createchallenge-scene]]
            [boldly.ios.scenes.edit-challenge :refer [edit-challenge-scene]]
            [boldly.ios.scenes.challengedetail :refer [challengedetail-scene]]
            [boldly.ios.scenes.chat :refer [chat-scene]]))

(defn nav-title [props]
  [header-title { :style (:nav-title styles)
                  :text-style (:nav-title-text styles)} 
                  [ui/text {:style (:nav-title-text styles)} 
                  (aget props "scene" "route" "title")]])

(defn nav-right-btn [right-btn]
  [ui/touchable
           {:style {:flex 1
                    :flex-direction "row"
                    :align-items "center"
                    :margin-horizontal 16
                    }
            :on-press #(do
                        (ui/dismiss-keyboard)
                        (if (fn? (:handler right-btn))
                          (:handler right-btn)
                          (dispatch (:handler right-btn))))}
           [ui/text
            {:style (merge (:nav-right-btn-text styles) (select-keys right-btn [:font-size]))}
            (:text right-btn)]])

(defn render-right-component [props]
  (let [nkey (keyword (aget props "scene" "route" "key"))
        nav-state (subscribe [:nav/by-key nkey])]
    (when-let [rbtn (:right-btn @nav-state)]
      (r/as-element (nav-right-btn rbtn)))))

(defn nav-left-btn []
  [ui/touchable
   {:style {:flex 1
            :flex-direction "row"
            :align-items "center"}
    :on-press #(dispatch [:nav/pop nil])}
   [ui/ionicon
    {:name "ios-arrow-back"
     :size 30
     :style {:margin-horizontal 16
             :margin-top 4
             :color (:royal colors)}}]])

(defn render-left-component [props]
  (let [idx (aget props "scene" "index")]
    (when (> idx 0)
      (r/as-element (nav-left-btn)))))

(defn header
  [props]
  [navigation-header
   (let [props-clj (js->clj props :keywordize-keys true)]
     (assoc
       props-clj
       :style (:nav-header styles)
       :render-title-component #(r/as-element (nav-title %))
       :render-right-component render-right-component
       :render-left-component render-left-component
       ))])

(defn router [props]
  (let [route (aget props "scene" "route")
        cid (aget route "cid")]
    (case (keyword (aget route "scene-type"))
      :challenges [challenges-scene]
      :createchallenge [createchallenge-scene]
      :edit-challenge [edit-challenge-scene cid]
      :challengedetail [challengedetail-scene cid]
      :chat [chat-scene cid]

    [challenges-scene])))

(defn nav-root []
  [ui/card-stack {
                  :on-navigate-back #(dispatch [:nav/pop nil])
                  :render-header    #(r/as-element (header %))
                  :navigation-state @(subscribe [:nav/state])
                  :style            {:flex 1}
                  :render-scene     #(r/as-element (router %))
                  }])
