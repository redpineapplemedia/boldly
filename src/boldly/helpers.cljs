(ns boldly.helpers
  (:require [reagent.core :as r]
            [re-frame.core :refer [subscribe dispatch]]
            [cljs-time.core :as ct]
            [cljs-time.format :as ctf]
            [cljs-time.coerce :as ctc]
            [schema.core :as s]
            [boldly.schema :as ms]
            [clojure.string :as string]))


; ****** Date & Date Format related **********************************************************

(defn plus-one-day
  [datetime]
  (ct/plus datetime (ct/days 1)))

(def us-formatter
  (ctf/formatter "MMM dd, yyyy"))

(def short-formatter
  (ctf/formatter "dd MMM"))

(def key-formatter
  (ctf/formatter "yyyy-MM-dd"))

(defn js-date->cljs-date
  [js-date]
  (if (integer? js-date)
    (ctc/from-long js-date)
    (ctc/from-date js-date)))

(defn date->key
  "gets a js Date object, returns a keyword like :2015-08-08"
  [datetime]
  (->> datetime
       js-date->cljs-date
       (ctf/unparse key-formatter)
       keyword))

(defn get-now-date []
  (doto (js/Date.)
    (.setMilliseconds 0)
    (.setSeconds 0)
    (.setMinutes 0)
    (.setHours 12)))

(defn get-date [add-days]
  (let [day (ct/plus (ctc/from-date (get-now-date)) (ct/days add-days))]
    (ctc/to-date day)))

(defn get-past-date [minus-days]
  (let [day (ct/minus (ctc/from-date (get-now-date)) (ct/days minus-days))]
    (ctc/to-date day)))

(defn to-us-format
  [d]
  (ctf/unparse us-formatter d))

(defn to-short-format
  [d]
  (ctf/unparse short-formatter d))

(defn at-noon [datetime]
  (js/Date. (.getFullYear datetime) (.getMonth datetime) (.getDate datetime) 12))

(defn end-after-or-equal-start?
  [start-datetime end-datetime]
  "returns true if end is after start or the same day as start.
  If end is before start, false is returned"
  (let [start (js-date->cljs-date start-datetime)
        end   (js-date->cljs-date end-datetime)]
    (or (ct/after? end start)
        (ct/equal? end start))))


; ****** Map related **********************************************************

(defn flatten-map
  ([form separator]
   (into {} (flatten-map form separator nil)))
  ([form separator pre]
   (mapcat (fn [[k v]]
             (let [prefix (if pre (str pre separator (str k)) (str k))]
               (if (map? v)
                 (flatten-map v separator prefix)
                 [[(keyword prefix) v]])))
           form)))

; ****** String related **********************************************************

(s/defn pluralized-str
  "given a number and a string
  returns the number plus the string in pluralized way
  examples:
  (pluralize 5 \"day\") ; -> \"5 days\"
  (pluralize 1 \"day\") ; -> \"1 day\"


  adds s to the string if number is not 1"
  [n :- s/Num
   s :- s/Str]
  (str n " " s (when (not= 1 n) "s")))

; ****** Challenge related **********************************************************

(s/defn running? :- s/Bool
  [today :- s/Inst
   {:keys [start-date end-date] :as c} :- ms/Challenge]
  (let [sd  (-> start-date ctc/from-long)
        ed  (-> end-date ctc/from-long (ct/plus (ct/days 1)))
        now (ctc/from-date today)]
    (ct/within? sd ed now)))

(s/defn upcoming? :- s/Bool
  [today :- s/Inst
   {:keys [start-date] :as c} :- ms/Challenge]
  (let [sd  (-> start-date ctc/from-long)
        now (ctc/from-date today)]
    (ct/after? sd now)))

(s/defn finished? :- s/Bool
  [today :- s/Inst
   {:keys [end-date id] :as c} :- ms/Challenge]
  (let [ed  (-> end-date ctc/from-long)
        now (ctc/from-date today)]
    (ct/before? ed now)))

(s/defn challenge->time-status :- ms/ChallengeTimeStatus
  [today :- s/Inst
   c :- ms/Challenge]
  (cond
    (upcoming? today c) :upcoming
    (finished? today c) :finished
    :else               :running))

(s/defn days-left :- [(s/one s/Int "left-days") (s/one s/Int "duration")]
  [today :- s/Inst
   {:keys [start-date end-date] :as c} :- ms/Challenge]
  (let [now (ctc/from-date today)
        sd  (-> start-date ctc/from-long)
        ed  (-> end-date ctc/from-long plus-one-day)
        diff-ne (ct/in-days (ct/interval (if (< now ed) now ed) ed))
        diff-se (ct/in-days (ct/interval sd ed))]
    [(min diff-ne diff-se) diff-se]))

(s/defn starts-in :- s/Int
  [today :- s/Inst
   {:keys [start-date end-date] :as c} :- ms/Challenge]
  (let [now (ctc/from-date today)
        sd  (-> start-date ctc/from-long)]
    (ct/in-days (ct/interval (min now sd) sd))))

(s/defn won? :- s/Bool
  [{:keys [goal]} :- ms/Challenge
   trackings :- ms/ChallengeTrackings]
  (let [sum (->> trackings vals (apply +))]
    (>= sum goal)))

(s/defn lost? :- s/Bool
  [today :- s/Inst
   {:keys [goal] :as c} :- ms/Challenge
   trackings :- ms/ChallengeTrackings]
  (let [sum (->> trackings vals (apply +))]
    (and
      (finished? today c)
      (< sum goal))))

(s/defn daily-goal-ratio :- [(s/one s/Int "tracking-today") (s/one s/Int "daily-goal")]
  [today :- s/Inst
   {:keys [goal] :as c} :- ms/Challenge
   trackings :- (s/maybe ms/ChallengeTrackings)]
  (let [date-key        (date->key today)
        tracking-today  (-> trackings date-key (or 0))
        days-left       (first (days-left today c))
        sum             (->> trackings vals (apply +))
        score-left      (- goal (- sum tracking-today))]
    [tracking-today
     (if (> score-left 0)
       (int (Math.ceil (/ score-left days-left)))
       0)]))

(s/defn daily-goal-reached? :- s/Bool
  [today :- s/Inst
   c :- ms/Challenge
   trackings :- ms/ChallengeTrackings]
  (let [[reached goal] (daily-goal-ratio today c trackings)]
    (>= reached goal)))

(s/defn challenge->goal-status :- ms/ChallengeGoalStatus
  [today :- s/Inst
   c :- ms/Challenge
   ts :- ms/ChallengeTrackings]
  (cond
    (won? c ts)                      :won
    (lost? today c ts)               :lost
    (daily-goal-reached? today c ts) :daily-reached))

(s/defn challenge->days-ratio :- [(s/one s/Int "running")
                                  (s/one s/Int "duration")]
  "receives a challenge and the today date
  returns a vector with
  [running-days duration-of-challenge]"
  [{:keys [start-date end-date] :as c} :- ms/Challenge
   today :- s/Inst]
  (let [now (-> today ctc/from-date plus-one-day)
        sd  (-> start-date ctc/from-long)
        ed  (-> end-date ctc/from-long plus-one-day)
        duration (ct/in-days (ct/interval sd ed))
        running (ct/in-days (ct/interval sd (if (>= now sd)
                                              now
                                              sd)))]
    [(min running duration) duration]))


(defn share-url->cid
  [url]
  (second (re-matches #".*?code=([^&]*).*" url)))
