# boldly

An example iOs app that uses:
- ReactNative
- ClojureScript
- Reagent
- re-frame
- re-natal
- firebase
- mixpanel for tracking

## Usage

first install all dependencies (scroll down to find them)
then:
```
git clone https://github.com/redpineapplemedia/boldly.git
cd boldly
npm install
re-natal use-figwheel
rlwrap lein figwheel ios
```
this will open a clojurescript repl

in a second terminal window (also in the project root) run:
```
react-native run-ios
```
Now get a coffee....

The iPhone simulator should be open now.
now press: `alt+ctrl+z` (or goto Hardware > Shake Gesture)
In the menu that pops up press "Debug JS Remotely"
This should open a chrome window where you can use the developer tools to debug the application.

In the iPhone simulator you can always press `apple + r` to restart the application.

To reset the state of the application go to the repl and run: `(re-frame.core/dispatch [:reset])`

## Dependencies
The version numbers are just showing with what versions I have tested it.

- [npm](https://www.npmjs.com) (4.3.0)
    - [Node.js](https://nodejs.org) (6.8.0)
- [react-native-cli](https://www.npmjs.com/package/react-native-cli) (2.0.1) (`npm install -g react-native-cli`)
- [Leiningen](http://leiningen.org) (2.7.1)
    - [Java 8](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
- [Xcode](https://developer.apple.com/xcode) (+ Command Line Tools) (7.3.1) (optional for Android)
    - [OS X](http://www.apple.com/osx) (10.11.4)
- [re-natal](https://github.com/drapanjanas/re-natal/) (0.3.4) (`npm install -g re-natal`)
- [rlwrap](http://macappstore.org/rlwrap/) (`brew install rlwrap`)